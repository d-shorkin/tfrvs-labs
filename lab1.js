// Подключаем методы для работы
const generate = require('./methods/generateQueue');
const nfdh = require('./methods/nfdh');
const ffdh = require('./methods/ffdh');

// Входные параметры

// Количесво задач
const n = 100000;
// Количесво элементарных машин
const maxEms = 1024;

// Генерируем очередь
let queue = generate(n, {maxM: maxEms});

// Запускаем NFDH
let nfdhResult = nfdh(queue, maxEms);
console.log(`NFDH time:\t${nfdhResult.time}ms`);
console.log(`NFDH height:\t${nfdhResult.finalH}`);
console.log(`NFDH levels:\t${nfdhResult.level}`);

// Запускаем FFDH
let ffdhResult = ffdh(queue, maxEms);
console.log(`FFDH time:\t${ffdhResult.time}ms`);
console.log(`FFDH height:\t${ffdhResult.finalH}`);
console.log(`FFDH levels:\t${ffdhResult.level}`);