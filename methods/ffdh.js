module.exports = function (data, maxW) {

    let time = -Date.now();

    // Сортируем очередь по убыванию времени
    data.sort((a, b)=> (b.time - a.time) );

    let result = [];
    let levels = [];
    let finalH = 0;

    for(let i = 0; i < data.length; i++){
        // Проверяем количество ЭМ на задачу,
        // если оно больше максимального кидаем ошибку
        if(data[i].ems > maxW){
            throw new Error(`Issue ${i} has so big rules`);
        }

        // Поиск уровня на который можно поставить задачу
        let level = null;
        for(let j = 0; j < levels.length; j++){
            // Пропускаем итерацию если уровень уже полностью занят
            if(levels[j] >= maxW) continue;
            if((levels[j] + data[i].ems) < maxW){
                level = j;
                break;
            }
        }

        // Если уровень не выбран его нужно создать
        if(level === null){
            level = levels.length;
            levels[level] = 0;
            result[level] = [];
        }

        // Увеличиваем финальное время выполнения
        // если на этом уровне еще нет элементов
        if(!result[level].length){
            finalH += data[i].time;
        }

        // Наконец добавляем элемент в массив
        // и прибовляем количество задействованных
        // элементарных машин на выбранном уровне
        result[level].push(data[i]);
        levels[level] += data[i].ems;
    }

    // Считаем время выполнения
    time += Date.now();

    // Возвращаем обьект с итогами выполненой работы
    return {time, finalH, level: levels.length, result};
};