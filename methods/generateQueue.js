function randInt(min, max) {
    return Math.round(min - 0.5 + Math.random() * (max - min + 1));
}


module.exports = function (n, options = {}) {
    options = copy = Object.assign({

        // Время выплнения задачи.
        minTime: 1,
        maxTime: 100,
        // Количество элементарных машин.
        minM: 1,
        maxM: 100

    }, options);

    let queue = [];

    for(let i = 0; i < n; i++){
        queue.push({
            time: randInt(options.minTime, options.maxTime),
            ems: randInt(options.minM, options.maxM)
        })
    }

    return queue;
};