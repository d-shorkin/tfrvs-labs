module.exports = function (data, maxW) {

    let time = -Date.now();

    // Сортируем очередь по убыванию времени
    data.sort((a, b)=> (b.time - a.time) );

    let result = [];
    let level = 0;
    let currentW = 0;
    let finalH = 0;

    for(let i = 0; i < data.length; i++){
        // Проверяем количество ЭМ на задачу,
        // если оно больше максимального кидаем ошибку
        if(data[i].ems > maxW){
            throw new Error(`Issue ${i} has so big rules`);
        }

        // Проверяем можно ли на этот уровень
        // добавить задачу или уже нет места
        if( (currentW + data[i].ems) > maxW){
            level++;
            currentW = 0;
        }

        // Если уровня нет то создаем его
        if(typeof result[level] === 'undefined'){
            result[level] = [];
        }

        // Увеличиваем финальное время выполнения
        // если на этом уровне еще нет элементов
        if(!result[level].length){
            finalH += data[i].time;
        }

        // Наконец добавляем элемент в массив
        // и прибовляем количество задействованных
        // элементарных машин на этом уровне
        result[level].push(data[i]);
        currentW += data[i].ems;
    }

    // Считаем время выполнения
    time += Date.now();

    // Возвращаем обьект с итогами выполненой работы
    return {time, finalH, level, result};
};